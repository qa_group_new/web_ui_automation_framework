# Python Web UI Automation Framework

Hybrid Custom Framework for Web UI Automation (Selenium)

### Tech Stack
1. Python 3.11
2. Selenium
3. Testing Framework : PyTest 
4. Reporting : Allure Report, PyTest HTML
5. Test Data : Excel
6. Parallel Execution : pytest-xdist

### Install packages
```
pip install selenium pytest pytest-html faker allure-pytest
```

### Freeze your package version
```
pip freeze > requirements.txt
```

### Install freeze package version
```
pip install -r requirements.txt
```

### Parallel execution
```
pip install pytest-xdist
```

### Work with Excel files
```
pip install openpyxl
```