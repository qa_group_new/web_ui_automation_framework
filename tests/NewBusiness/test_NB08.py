import logging
import os
from dotenv import load_dotenv
import pytest

# Load Page elements
from src.pageElements.login_Page import *
from src.pageElements.homepage_Page import *
from src.pageElements.create_appointment_Page import *
from src.pageElements.verify_appointment_details_Page import *
from src.pageElements.logout_Page import *

# Load utilities
from src.utils.save_screenshot import save_screenshot
from src.utils.excel_reader import read_excel_file, get_value
from src.constants.url_repository import *

LOGGER = logging.getLogger(__name__)


@pytest.mark.usefixtures("driver")
def test_NB08(driver):
    try:
        # Read the Excel file
        testdata = read_excel_file(__file__)

        # Test Steps
        # Open homepage URL
        open_homepage(driver, testdata)

        # Click on Make Appointment button
        click_make_appointment_btn(driver)

        # Login page
        login(driver)

        # Create Appointment page
        create_appointment(driver, testdata)

        # Verify Appointment Details
        verify_appointment_details(driver, testdata)

        # Logout
        logout(driver)

    except Exception:
        # Capture and save screenshot on any exception
        save_screenshot(driver, __file__, __name__)

        # Re-raise the exception to mark the test as failed
        raise


# Open homepage URL
def open_homepage(driver, testdata):
    # Open homepage
    driver.get(str(homepage_url()))

    # Assert title is correct
    assert driver.title == get_value(testdata, "title", 1), \
        "Invalid title. Expected = " + get_value(testdata, "title", 1) + " Actual = " + driver.title


# Click on Make Appointment button
def click_make_appointment_btn(driver):
    # Click on Make Appointment button
    make_appointment_btn(driver).click()

    # Assert URL is correct
    assert driver.current_url == str(login_url()), "Invalid URL. Expected = " + str(
        login_url()) + " Actual = " + driver.current_url


# Login page
def login(driver):
    # Take environment variables from .env.
    load_dotenv()

    # Enter username
    username(driver).send_keys(os.getenv("UNAME"))

    # Enter password
    password(driver).send_keys(os.getenv("PWD"))

    # Click on login button
    login_btn(driver).click()

    # Assert URL is correct
    assert driver.current_url == str(create_appointment_url()), "Invalid URL. Expected = " + str(
        create_appointment_url()) + " Actual = " + driver.current_url


# Create Appointment page
def create_appointment(driver, testdata):
    # Select facility
    facility(driver).select_by_visible_text(get_value(testdata, "facility", 1))

    # Check Readmission checkbox
    readmission(driver).click()

    # Assert Readmission checkbox is checked
    assert readmission(driver).get_attribute("checked"), "Checkbox is not checked"

    # Choose the Healthcare Program
    healthcare_program(driver).click()

    # Enter the visit date
    visit_date(driver).send_keys("01/01/2024")

    # Enter the comments
    comments(driver).send_keys(get_value(testdata, "comments", 1))

    # Click on Book Appointment button
    book_appointment_btn(driver).click()

    # Assert URL is correct
    assert driver.current_url == str(verify_appointment_details_url()), "Invalid URL. Expected = " + str(
        verify_appointment_details_url()) + " Actual = " + driver.current_url


# Verify Appointment Details
def verify_appointment_details(driver, testdata):
    # Assert Facility label is correct
    assert lbl_facility(driver).text == get_value(testdata, "facility", 1)

    # Assert Hospital Readmission label is correct
    assert lbl_hospital_readmission(driver).text == get_value(testdata, "lbl_hospital_readmission", 1)

    # Assert Healthcare Program label is correct
    assert lbl_program(driver).text == get_value(testdata, "lbl_program", 1)

    # Assert Healthcare Program label is correct
    assert lbl_visit_date(driver).text == get_value(testdata, "visit_date", 1)

    # Assert Comments label is correct
    # assert lbl_comment(driver).text == get_value(testdata, "comments", 1)
    assert lbl_comment(driver).text == "Demo"


# Logout
def logout(driver):
    # Click on Options
    options(driver).click()

    # Click on Logout
    logout_link(driver).click()

    # Assert URL is correct
    assert driver.current_url == str(homepage_url()), "Invalid URL. Expected = " + str(
        homepage_url()) + " Actual = " + driver.current_url
