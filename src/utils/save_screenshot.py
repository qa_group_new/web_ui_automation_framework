import os


# Save screenshot
def save_screenshot(driver, file_name, test_name_full):

    # Get the directory of the current script (testcase file)
    script_directory = os.path.dirname(os.path.abspath(file_name))

    # Construct the relative path to the screenshots folder
    screenshot_path = os.path.join(script_directory, "..", "..", "screenshots")

    # Create screenshots folder, if not already exists
    os.makedirs(screenshot_path, exist_ok=True)

    # Get filename
    test_file_name = os.path.basename(test_name_full)
    parts = test_file_name.split('ss.')
    test_name = parts[1]

    # Construct the relative path for screenshot file
    screenshot_file = os.path.join(screenshot_path, f"{test_name}.png")

    # Save screenshot
    driver.save_screenshot(screenshot_file)

    # Prints screenshot location
    print(f"Screenshot saved at: {screenshot_file}")
