import os
import pandas as pd


def read_excel_file(file_name):
    # Get the directory of the current script (testcase file)
    script_directory = os.path.dirname(os.path.abspath(file_name))

    # Get filename
    testdata_file_name = os.path.basename(file_name)
    parts = testdata_file_name.split('.')
    testdata = parts[0]

    # Construct the relative path to the Excel file
    excel_file_path = os.path.join(script_directory, "..", "..", "src", "resources", "testdata", f"{testdata}_testdata.xlsx")

    # Read the Excel file into a DataFrame
    try:
        df = pd.read_excel(excel_file_path)
        return df.to_dict(orient='list')
    except FileNotFoundError:
        print(f"File '{excel_file_path}' not found.")


def get_value(data, column_name, row_number):
    # Fetch the value based on the provided column name and row number
    try:
        value = data[column_name][row_number - 1]
        return value
    except KeyError:
        print(f"Column '{column_name}' not found.")
    except IndexError:
        print(f"Row {row_number} not found.")
