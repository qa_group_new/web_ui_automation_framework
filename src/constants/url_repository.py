# Homepage URL
def homepage_url():
    url = "https://katalon-demo-cura.herokuapp.com/"
    return url


# Dashboard URl
def login_url():
    url = "https://katalon-demo-cura.herokuapp.com/profile.php#login"
    return url


# Create Appointment URl
def create_appointment_url():
    url = "https://katalon-demo-cura.herokuapp.com/#appointment"
    return url


# Verify Appointment Details
def verify_appointment_details_url():
    url = "https://katalon-demo-cura.herokuapp.com/appointment.php#summary"
    return url
