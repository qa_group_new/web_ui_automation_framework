from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select


# Facility element
def facility(driver):
    facility_ele = Select(driver.find_element(By.XPATH, "//select[@id='combo_facility']"))
    return facility_ele


# Re-admission element
def readmission(driver):
    readmission_ele = driver.find_element(By.XPATH, "//input[@id='chk_hospotal_readmission']")
    return readmission_ele


# Healthcare Program element
def healthcare_program(driver):
    healthcare_program_ele = driver.find_element(By.XPATH, "//input[@id='radio_program_medicaid']")
    return healthcare_program_ele


# Visit Date element
def visit_date(driver):
    visit_date_ele = driver.find_element(By.XPATH, "//input[@id='txt_visit_date']")
    return visit_date_ele


# Comments element
def comments(driver):
    comments_ele = driver.find_element(By.XPATH, "//textarea[@id='txt_comment']")
    return comments_ele


# Book Appointment button element
def book_appointment_btn(driver):
    book_appointment_btn_ele = driver.find_element(By.XPATH, "//button[@id='btn-book-appointment']")
    return book_appointment_btn_ele
