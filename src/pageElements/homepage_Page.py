from selenium.webdriver.common.by import By


# Make Appointment button
def make_appointment_btn(driver):
    make_appointment_btn_ele = driver.find_element(By.XPATH, "//a[@id='btn-make-appointment']")
    return make_appointment_btn_ele
