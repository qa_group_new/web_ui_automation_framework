from selenium.webdriver.common.by import By


# Options element
def options(driver):
    options_ele = driver.find_element(By.XPATH, "//i[@class='fa fa-bars']")
    return options_ele


# Logout Link element
def logout_link(driver):
    logout_link_ele = driver.find_element(By.XPATH, "//a[normalize-space()='Logout']")
    return logout_link_ele
