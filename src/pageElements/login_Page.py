from selenium.webdriver.common.by import By


# Username element
def username(driver):
    username_ele = driver.find_element(By.XPATH, "//input[@id='txt-username']")
    return username_ele


# Password element
def password(driver):
    password_ele = driver.find_element(By.XPATH, "//input[@id='txt-password']")
    return password_ele


# Login button element
def login_btn(driver):
    login_btn_ele = driver.find_element(By.XPATH, "//button[@id='btn-login']")
    return login_btn_ele
