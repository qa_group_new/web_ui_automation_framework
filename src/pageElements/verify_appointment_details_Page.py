from selenium.webdriver.common.by import By


# Facility element
def lbl_facility(driver):
    lbl_facility_ele = driver.find_element(By.XPATH, "//p[@id='facility']")
    return lbl_facility_ele


# Re-admission element
def lbl_hospital_readmission(driver):
    lbl_hospital_readmission_ele = driver.find_element(By.XPATH, "//p[@id='hospital_readmission']")
    return lbl_hospital_readmission_ele


# Healthcare Program element
def lbl_program(driver):
    lbl_program_ele = driver.find_element(By.XPATH, "//p[@id='program']")
    return lbl_program_ele


# Visit Date element
def lbl_visit_date(driver):
    lbl_visit_date_ele = driver.find_element(By.XPATH, "//p[@id='visit_date']")
    return lbl_visit_date_ele


# Comments element
def lbl_comment(driver):
    lbl_comment_ele = driver.find_element(By.XPATH, "//p[@id='comment']")
    return lbl_comment_ele


# Book Appointment button element
def book_appointment_btn(driver):
    book_appointment_btn_ele = driver.find_element(By.XPATH, "//button[@id='btn-book-appointment']")
    return book_appointment_btn_ele
